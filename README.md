## PROJECT ##

* ID: **S**udoku!**S**OLVER
* Contact: development@schepsen.eu

## USAGE ##

To compile this project use CMAKE

mkdir build && cd build && cmake .. & make

## REQUIREMENTS ##

* CMAKE 2.8.11 http://www.cmake.org/cmake/resources/software.html

## CHANGELOG ##

### Sudoku!SOLVER 1.0, updated @ 2016-01-23 ###

* Initial release
