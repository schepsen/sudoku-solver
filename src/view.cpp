#include "moc_view.cpp"
#include "sudoku.h"
#include <vector>
#include <QFile>
#include <QDebug>
#include <QElapsedTimer>
#include <QDial>
#include <QDate>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QString>

#define QSTR(x) QString::number(x)

#define MAJOR_VERSION "1"
#define MINOR_VERSION "0"

#define BUILD QDate::currentDate().toString("yyyyMMdd")

View::View(QWidget* parent)
    :
    QMainWindow(parent),

    ui(new Ui::View)
{
    this->ui->setupUi(this);

    QStandardItemModel* model = new QStandardItemModel(9, 9, this);

    for (int i = 0; i < 9; ++i)
    {
        for (int j = 0; j < 9; ++j)
        {
            model->setData(model->index(i, j), Qt::AlignCenter, Qt::TextAlignmentRole);

            if((((i > 2) && (i < 6)) && ((j < 3) | (j > 5))) || (((j > 2) && (j < 6)) && ((i < 3) | (i > 5))))
            {
                model->setData(model->index(i, j), QColor(240, 240, 240, 150), Qt::BackgroundColorRole);
            }
        }
    }

    this->ui->tableView->setModel(model);

    QObject::connect(ui->dial, &QDial::sliderMoved, this, &View::onDifficultyChanged);
    QObject::connect(ui->solve, &QPushButton::clicked, this, &View::onSolveClicked);
}

void View::onDifficultyChanged(int value)
{
    switch (value)
    {
        case 1:
            ui->lineEdit->setText("Einfach");
            break;
        case 2:
            ui->lineEdit->setText("Moderat");
            break;
        case 3:
            ui->lineEdit->setText("Anspruchsvoll");
            break;
        case 4:
            ui->lineEdit->setText("Teuflisch");
            break;
        default:
            ui->lineEdit->setText("Sehr Einfach");
            break;
    }
}

void View::onSolveClicked()
{
    QSudokuBoard<> input(9, QSudokuVec<>(9, 0));
    auto table = this->ui->tableView->model();

    for(int i = 0; i < 9; ++i)
    {
        for(int j = 0; j < 9; ++j)
        {
            input[i][j] = table->data(table->index(i, j), Qt::DisplayRole).toInt();
        }
    }

/*
    QFile f; f.setFileName("sudoku.txt");

    if(f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        int i = 0;

        while(!f.atEnd())
        {
            QStringList record = QString(f.readLine()).split(" ");
            for(int j = 0; j < 9; ++j)
            {
                input[i][j] = record.at(j).toInt();
                if(input[i][j])
                {
                    table->setData(table->index(i, j), QColor(239, 239, 255, 180), Qt::BackgroundColorRole);
                }
            }
            ++i;
        }
        f.close();
    }
*/

    Sudoku game(input); QElapsedTimer timer; timer.start();

    game.solve();

    qDebug() << timer.nsecsElapsed() / 1000000.0;

    for(int i = 0; i < 9; ++i)
    {
        for(int j = 0; j < 9; ++j)
        {
            QString str = (game.solution()[i][j] == 0) ? "" : QString::number(game.solution()[i][j]);
            table->setData(table->index(i, j), str, Qt::DisplayRole);
        }
    }
}

View::~View() { delete ui; }
