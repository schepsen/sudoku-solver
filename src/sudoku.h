#ifndef SUDOKU_H
#define SUDOKU_H

#include <cassert>
#include <iostream>
#include <vector>
#include <unordered_set>

template<typename T = unsigned int>
using QSudokuVec = std::vector<T>;

template<typename T = QSudokuVec<> >
using QSudokuBoard = std::vector<T>;

// FIXED SUDOKU'S BOX
#define IMMUTABLE 1
// DIMENSION OF THE SUDOKU (NxN)
#define N 9
// STANDARD STATUS
#define OP_DEFAULT 0x10
// SUDOKU IS NOT SOLVEABLE
#define OP_INVALID 0x11
// SUDOKU IS SOLVED SUCCESSFULLY
#define OP_SUCCESS 0x12

#define MSG(s) std::cout << s << std::endl;

#define BLOCK(x) (x / 3) * 3

typedef std::unordered_set<unsigned> QSolution;

/**
 * The Sudoku CLASS
 */

class Sudoku
{
private:
    QSudokuBoard<> board;
    unsigned op;
    unsigned uncovered;
    QSudokuBoard<QSudokuVec<QSolution> > possibilities;
public:
    Sudoku(QSudokuBoard<>& game)
    :
    board(game),
    op(OP_DEFAULT),
    uncovered(0),
    possibilities(QSudokuBoard<QSudokuVec<QSolution> >(
            N,
            QSudokuVec<QSolution>(
                    N,
                    QSolution({ 1, 2, 3, 4, 5, 6, 7, 8, 9 }))
                    )
            )
    {
        for(int i = 0; i < N; ++i)
        {
            for(int j = 0; j < N; ++j)
            {
                if(this->board[i][j])
                {
                    if(!this->isValid(i, j, this->board[i][j]))
                    {
                        op = OP_INVALID;

                        MSG("> The Sudoku is invalid!");

                        return;
                    }

                    this->uncover(i, j, this->board[i][j]);
                }
            }
        }
    }

    /**
     * This Function uncovers a field with coordinates (i, j)
     *
     * <b>i</b> stands for a row<br>
     * <b>j</b> stands for a column<br>
     *
     * <b>num</b> stands for an uncovered number
     */

    void uncover(int i, int j, unsigned num)
    {
        if(++this->uncovered == (N*N))
        {
            this->op = OP_SUCCESS;
        }

        if(!this->board[i][j]) this->board[i][j] = num;

        this->possibilities[i][j].clear();
    }

    /**
     * This Function returns the solution for the given SUDOKU
     */

    QSudokuBoard<>& solution(void) { return this->board; }

    /**
     * This Function solves the given SUDOKU
     */

    void solve(void)
    {
        if(this->op == OP_INVALID) return;
        /**
         * Phase 1
         * Name: Candidate Search
         */
        this->candidateSearch();
        /**
         * Check if SUDOKU already solved
         */
        if(this->op == OP_SUCCESS) return;
        /**
         * Phase 2
         * Name: Backtracking
         */
        this->backtracking();
    }

    /**
     * This Function tries to solve the SUDOKU with Candidate Search
     */

    void candidateSearch(void)
    {
        bool running = true;
        while(running)
        {
            running = false;

            for (int row = 0; row < N; ++row)
            {
                for (int col = 0; col < N; ++col)
                {
                    if(!this->board[row][col])
                    {
                        QSolution& s = this->possibilities[row][col];

                        for (int x = 0; x < N; ++x)
                        {
                            if(s.count(this->board[row][x]) > 0)
                            {
                                running = true;

                                s.erase(this->board[row][x]);
                            }
                            if(s.count(this->board[x][col]) > 0)
                            {
                                running = true;

                                s.erase(this->board[x][col]);
                            }
                        }

                        for(int x = BLOCK(row); x < BLOCK(row) + 3; ++x)
                        {
                            for(int y = BLOCK(col); y < BLOCK(col) + 3; ++y)
                            {
                                if(s.count(this->board[x][y]) > 0)
                                {
                                    running = true;

                                    s.erase(this->board[x][y]);
                                }
                            }
                        }

                        if(s.size() > 1)
                        {
                            for(auto it = s .begin(); it != s.end(); ++it)
                            {
                                int count = 0;

                                for (int r = BLOCK(row), c = BLOCK(col); r < BLOCK(row) + 3; ++r, ++c)
                                {
                                    for(int i = 0; i < N; ++i)
                                    {
                                        if((r != row) && (*it == this->board[r][i]))
                                        {
                                            count++;
                                        }

                                        if((c != col) && (*it == this->board[i][c]))
                                        {
                                            count++;
                                        }
                                    }
                                }

                                if(count == 4) { this->uncover(row, col, *it); break; }
                            }
                        }
                        else
                        {
                            this->uncover(row, col, *s.begin());
                        }
                    }
                }
            }
        }
    }

    /*
     * This Function visits nodes from up to down, from left to right
     */

    bool backtracking(int row = 0, int col = 0)
    {
        if(row >= N)
        {
            row = 0; col++;
        }

        // We're done! The Sudoku is solved successfully!

        if(col >= N) return true;

        if(this->possibilities[row][col].size())
        {
            for(auto item : this->possibilities[row][col])
            {
                if(this->isValid(row, col, item))
                {
                    this->board[row][col] = item;

                    if(this->backtracking(row + 1, col)) return true;
                }
            }

            this->board[row][col] = 0; // A wrong way! Go BACK!
        }
        else
        {
            if(this->backtracking(row + 1, col)) return true;
        }

        return false;
    }

    /**
     * This Function checks the validity of the given number for the <b>row</b>, the <b>column</b> and the <b>grid</b>
     */

    bool isValid(int row, int col, unsigned num)
    {
        assert(row < N); assert(col < N); assert((num > 0) && (num <= N));

        for(int i = 0; i < N; ++i)
        {
            if((this->board[row][i] == num) && (i != col))
            {
                return false;
            }

            if((this->board[i][col] == num) && (i != row))
            {
                return false;
            }
        }

        for(int i = BLOCK(row); i < BLOCK(row) + 3; ++i)
        {
            for(int j = BLOCK(col); j < BLOCK(col) + 3; ++j)
            {
                if((this->board[i][j] == num) && (i != row) && (j != col)) { return false; }
            }
        }

        return true;
    }
};

#endif /* SUDOKU_H */
