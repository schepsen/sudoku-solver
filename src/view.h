#ifndef VIEW_H
#define VIEW_H

#include <QMainWindow>
#include <QWidget>

#include "ui_view.h"

namespace Ui
{
    class View;
}

class View
    :
    public QMainWindow
{
    Q_OBJECT

private:

    Ui::View* ui;

public:

    explicit View(QWidget* parent = 0);
    ~View();

public slots:

    void onSolveClicked(void);
    void onDifficultyChanged(int);
};

#endif /* VIEW_H */
